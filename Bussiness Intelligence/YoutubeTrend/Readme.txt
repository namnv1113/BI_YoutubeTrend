This is the final report submission for the Decision Support System course by
	14520560 - Nguyễn Việt Nam
	14520612 - Trần Trí Nguyên
This is the BI report for YouTube Trend videos
The contents of the submission is as follow:
	\Scripts\	- Contains Microsoft SQL script that is used in SSIS process for database and tables creation & deletion
			\CreateDatabase.sql - Script to create the database
			\CreateDatabaseObjects.sql - Script to create tables and constraints (required to run the CreateDatabase.sql first)
			\DeleteData - Delete all data for the database
	\Datasets\	- Conrains the dataset used for the BI project (online link: www.kaggle.com/datasnaek/youtube-new)
	\SSIS\ 		- Contains our works on the ETL process using SSIS 
		 \ISP_YoutubeTrend.sln 	- A SSIS project that is tested and run on Visual Studio 2015
	\SSAS\ 		- Contains our works on the analysis and mining process using SSAS 
		 \MultidimensionalProject_YoutubeTrend.sln 	- A SSAS project that is tested and run on Visual Studio 2015
		 \ExcelPivot.xlsx							- A excel file demonstrate the use of Excel pivot table
		 \MDX.txt									- A copy of the MDX queries that is used to browse the cube
	\SSRS
		 \SSRS_YoutubeTrend.sln	 - A SSRS project that is tested and run on Visual Studio 2015
		 \Reports\				 - Contains reports that we make using SSRS
	\Report.docx		- Contains detailed report about our BI project

Aside from the submission, we provide some videos about the process we create and run our BI project
The videos can be found at: 
	Here:	https://drive.google.com/file/d/1qIR4DvIWzVvU9YgFxF9ko94LHrovE1Uo/view
	Or:		https://drive.google.com/drive/folders/1bTlikCmIYVHcyJpq8evNvahjUEQW5Uhh?usp=sharing
	