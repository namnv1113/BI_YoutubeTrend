USE [YoutubeTrend]
GO
/****** Object:  Table [dbo].[Dim_Category]    Script Date: 6/5/2018 7:20:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_Category](
	[category_id] [int] NOT NULL,
	[category] [varchar](50) NULL,
 CONSTRAINT [PK_Dim_Category] PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_Channel]    Script Date: 6/5/2018 7:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_Channel](
	[channel_id] [int] IDENTITY(1,1) NOT NULL,
	[channel_title] [nvarchar](255) NULL,
 CONSTRAINT [PK__Dim_Chan__2D0861AB83F31D94] PRIMARY KEY CLUSTERED 
(
	[channel_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_CommentStatus]    Script Date: 6/5/2018 7:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_CommentStatus](
	[comments_disabled] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[comments_disabled] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_ErrorStatus]    Script Date: 6/5/2018 7:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_ErrorStatus](
	[video_error_or_removed] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[video_error_or_removed] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_PublishTime]    Script Date: 6/5/2018 7:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_PublishTime](
	[publish_time] [datetime] NOT NULL,
	[publish_year] [int] NULL,
	[publish_month] [int] NULL,
	[publish_day] [int] NULL,
	[publish_hour] [int] NULL,
	[publish_minute] [int] NULL,
	[publish_second] [int] NULL,
 CONSTRAINT [PK_Dim_PublishTime] PRIMARY KEY CLUSTERED 
(
	[publish_time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_RatingStatus]    Script Date: 6/5/2018 7:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_RatingStatus](
	[ratings_disabled] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ratings_disabled] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_Region]    Script Date: 6/5/2018 7:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_Region](
	[region_ID] [nvarchar](2) NOT NULL,
	[region_fullName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Dim_Region] PRIMARY KEY CLUSTERED 
(
	[region_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_Video]    Script Date: 6/5/2018 7:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_Video](
	[video_id] [nvarchar](255) NOT NULL,
	[channel_id] [int] NULL,
	[category_id] [int] NULL,
	[publish_time] [datetime] NULL,
	[tags] [nvarchar](max) NULL,
	[comments_disabled] [bit] NULL,
	[ratings_disabled] [bit] NULL,
	[video_error_or_removed] [bit] NULL,
	[title] [nvarchar](300) NULL,
 CONSTRAINT [PK_Dim_Video] PRIMARY KEY CLUSTERED 
(
	[video_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_VideoFailure]    Script Date: 6/5/2018 7:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_VideoFailure](
	[video_id] [nvarchar](255) NULL,
	[title] [nvarchar](300) NULL,
	[channel_title] [nvarchar](255) NULL,
	[category_id] [int] NULL,
	[publish_time] [datetime] NULL,
	[tags] [nvarchar](max) NULL,
	[comments_disabled] [bit] NULL,
	[ratings_disabled] [bit] NULL,
	[video_error_or_removed] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Fact_VideoTrendStatistics]    Script Date: 6/5/2018 7:20:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fact_VideoTrendStatistics](
	[region] [nvarchar](2) NOT NULL,
	[video_id] [nvarchar](255) NOT NULL,
	[trending_date_count] [numeric](18, 0) NULL,
	[views] [float] NULL,
	[likes] [float] NULL,
	[dislikes] [float] NULL,
	[comment_count] [float] NULL,
 CONSTRAINT [PK_Fact_VideoTrendStatistics_1] PRIMARY KEY CLUSTERED 
(
	[region] ASC,
	[video_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Fact_VideoTrendStatisticsFailure]    Script Date: 6/5/2018 7:20:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fact_VideoTrendStatisticsFailure](
	[region] [nvarchar](2) NULL,
	[video_id] [nvarchar](255) NULL,
	[trending_date_count] [numeric](18, 0) NULL,
	[views] [float] NULL,
	[likes] [float] NULL,
	[dislikes] [float] NULL,
	[comment_count] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mining_GoodTimeToUpload]    Script Date: 6/5/2018 7:20:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mining_GoodTimeToUpload](
	[region] [nvarchar](2) NOT NULL,
	[video_id] [nvarchar](255) NOT NULL,
	[category_id] [int] NULL,
	[views] [float] NULL,
	[likes] [float] NULL,
	[dislikes] [float] NULL,
	[comment_count] [float] NULL,
	[publish_month] [int] NULL,
	[publish_day] [int] NULL,
	[normal2trending_interval] [int] NULL,
	[publish_hour] [int] NULL,
	[trending_date] [datetime] NULL,
	[trendingdelay_category] [nvarchar](20) NULL,
	[rowid] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Mining_GoodTimeToUpload_1] PRIMARY KEY CLUSTERED 
(
	[rowid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Dim_Video]  WITH CHECK ADD  CONSTRAINT [FK_Dim_Video_Dim_Category] FOREIGN KEY([category_id])
REFERENCES [dbo].[Dim_Category] ([category_id])
GO
ALTER TABLE [dbo].[Dim_Video] CHECK CONSTRAINT [FK_Dim_Video_Dim_Category]
GO
ALTER TABLE [dbo].[Dim_Video]  WITH CHECK ADD  CONSTRAINT [FK_Dim_Video_Dim_Channel] FOREIGN KEY([channel_id])
REFERENCES [dbo].[Dim_Channel] ([channel_id])
GO
ALTER TABLE [dbo].[Dim_Video] CHECK CONSTRAINT [FK_Dim_Video_Dim_Channel]
GO
ALTER TABLE [dbo].[Dim_Video]  WITH CHECK ADD  CONSTRAINT [FK_Dim_Video_Dim_CommentStatus] FOREIGN KEY([comments_disabled])
REFERENCES [dbo].[Dim_CommentStatus] ([comments_disabled])
GO
ALTER TABLE [dbo].[Dim_Video] CHECK CONSTRAINT [FK_Dim_Video_Dim_CommentStatus]
GO
ALTER TABLE [dbo].[Dim_Video]  WITH CHECK ADD  CONSTRAINT [FK_Dim_Video_Dim_ErrorStatus] FOREIGN KEY([video_error_or_removed])
REFERENCES [dbo].[Dim_ErrorStatus] ([video_error_or_removed])
GO
ALTER TABLE [dbo].[Dim_Video] CHECK CONSTRAINT [FK_Dim_Video_Dim_ErrorStatus]
GO
ALTER TABLE [dbo].[Dim_Video]  WITH CHECK ADD  CONSTRAINT [FK_Dim_Video_Dim_PublishTime] FOREIGN KEY([publish_time])
REFERENCES [dbo].[Dim_PublishTime] ([publish_time])
GO
ALTER TABLE [dbo].[Dim_Video] CHECK CONSTRAINT [FK_Dim_Video_Dim_PublishTime]
GO
ALTER TABLE [dbo].[Dim_Video]  WITH CHECK ADD  CONSTRAINT [FK_Dim_Video_Dim_RatingStatus] FOREIGN KEY([ratings_disabled])
REFERENCES [dbo].[Dim_RatingStatus] ([ratings_disabled])
GO
ALTER TABLE [dbo].[Dim_Video] CHECK CONSTRAINT [FK_Dim_Video_Dim_RatingStatus]
GO
ALTER TABLE [dbo].[Fact_VideoTrendStatistics]  WITH CHECK ADD  CONSTRAINT [FK_Fact_VideoTrendStatistics_Dim_Region] FOREIGN KEY([region])
REFERENCES [dbo].[Dim_Region] ([region_ID])
GO
ALTER TABLE [dbo].[Fact_VideoTrendStatistics] CHECK CONSTRAINT [FK_Fact_VideoTrendStatistics_Dim_Region]
GO
ALTER TABLE [dbo].[Fact_VideoTrendStatistics]  WITH CHECK ADD  CONSTRAINT [FK_Fact_VideoTrendStatistics_Dim_Video] FOREIGN KEY([video_id])
REFERENCES [dbo].[Dim_Video] ([video_id])
GO
ALTER TABLE [dbo].[Fact_VideoTrendStatistics] CHECK CONSTRAINT [FK_Fact_VideoTrendStatistics_Dim_Video]
GO
ALTER TABLE [dbo].[Mining_GoodTimeToUpload]  WITH CHECK ADD  CONSTRAINT [FK_Mining_GoodTimeToUpload_Dim_Category] FOREIGN KEY([category_id])
REFERENCES [dbo].[Dim_Category] ([category_id])
GO
ALTER TABLE [dbo].[Mining_GoodTimeToUpload] CHECK CONSTRAINT [FK_Mining_GoodTimeToUpload_Dim_Category]
GO
ALTER TABLE [dbo].[Mining_GoodTimeToUpload]  WITH CHECK ADD  CONSTRAINT [FK_Mining_GoodTimeToUpload_Dim_Region] FOREIGN KEY([region])
REFERENCES [dbo].[Dim_Region] ([region_ID])
GO
ALTER TABLE [dbo].[Mining_GoodTimeToUpload] CHECK CONSTRAINT [FK_Mining_GoodTimeToUpload_Dim_Region]
GO
USE [master]
GO
ALTER DATABASE [YoutubeTrend] SET  READ_WRITE 
GO
