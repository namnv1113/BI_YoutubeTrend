select * 
from video
where video_id in (select video_id
					from Video 
					group by video_id
					having count(distinct title) > 1)
order by video_id